import React from "react";
import rasm from "../../image/plx-dark-default.jpg";
import "./AboutMe.scss";
import Comment from "../Comment/Comment";
import Logo from "../Logo/Logo";
import team_1 from "../../image/team-1.JPG";
import team_2 from "../../image/team-2.JPG";
import team_3 from "../../image/team-3.JPG";
import team_4 from "../../image/team-4.JPG";
import team_5 from "../../image/team-5.JPG";
import team_6 from "../../image/team-6.JPG";
import team_7 from "../../image/team-7.JPG";
import team_8 from "../../image/team-8.JPG";
import main_3 from "../../image/Main-3.jpg"
import o_nas from "../../image/o-nas-img.jpg"


const AboutMe = () => {




  return (
    <>
      <section className="about-me-section">
        <div className="section-about-me-img">
          <img
            src={main_3}
            alt="img"
          />
        </div>
        <div className="container">
          <div className="row">
            <div className="col-7-lg col-md-7 col-sm-12 about-me-text">
              <h3> О нашей компании</h3>
              <p>
                ООО «НАВОИ КОНИ СЕРВИС» создано в 2009 году для выполнения
                строительных работ на промышленных объектах с высоким уровнем
                потенциальной опасности. Течении тринадцать лет компания
                подписала более 635 контрактов. Сегодня в компании работает 85
                инженеров и техников и более 240 сотрудников. Главной целью
                компании является предоставление безопасных, быстрых и
                качественных услуг.
              </p>
              <p>
                Navoiy Kon Service – одна из ведущих и быстро развивающихся
                инженерной строительной компаний с 13 летним опытом на рынке.
                Деятельность компании направлена на реализацию уникальных
                проектов химической, энергетической, легкой промышленности,
                строительства повышенной опасности, отличается быстрым,
                грамотным подходом и высоким качеством. Нашей основной задачей
                является не только качественное выполнение строительно-монтажных
                работ и развитие эффективности производства, но и обеспечение
                своевременных расчетов и сервисного обслуживания.
              </p>
            </div>
            <div className="col-5-lg col-md-5 col-sm-12 about-me-img">
              <img
                src={o_nas}
                alt="img"
              />
            </div>
          </div>
        </div>
        <div>
          <div
            className="col back-img"
            style={{ backgroundImage: `url(${rasm})` }}
          >
            <div className="overlay">
              <div className="container">
                <h2>Почему нам стоит доверять</h2>
                <div className="overlay-text">
                  <h3>Сервис</h3>
                  <p>
                    Мы обеспечиваем индивидуальный подход к каждому клиенту с
                    учетом его возможностей и пожеланий при постройке объекта.
                  </p>
                </div>
                <div className="overlay-text">
                  <h3>Прозрачность</h3>
                  <p>
                    Мы ничего не скрываем от своих заказчиков. Наши клиенты
                    могут наблюдать за ходом строительства в режиме 24/7.
                  </p>
                </div>
                <div className="overlay-text">
                  <h3>Качество</h3>
                  <p>
                    Наша цель не объем строительства, а его качество. Все этапы
                    строительства проходят стандартизацию и соответствуют
                    требованиям Градостроительного кодекса РУз.
                  </p>
                </div>
                <div className="overlay-text">
                  <h3>Доверие</h3>
                  <p>
                    Наша компания зарекомендовала себя на строительном рынке как
                    один из надежных строителей. Мы помогаем решить самые разные
                    вопросы и постоянно взаимодействуем с заказчиками.
                  </p>
                </div>
                <div className="overlay-text">
                  <h3>Инновации</h3>
                  <p>
                    При реализации мы используем только лучшие решения и
                    передовые технологии, будь то сырье для строительства или
                    архитектура здания.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="about-me-comment">
          <Comment />
        </div>

        <div className="container">
          <div className="row">
            <h1>Наша команда</h1>
            <div className="col-lg-3 col-md-3 col-sm-6 about-me-person">
              <img src={team_2} alt="img" />
              <h3>Каюмов Сабиржан Алиевич</h3>
              <p>Директор</p>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-6 about-me-person">
              <img src={team_3} alt="img" />
              <h3>Назарматов Равшан Абдумажидович </h3>
              <p>Заместитель Директора</p>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-6 about-me-person">
              <img src={team_4} alt="img" />
              <h3>Шерназаров Музаффар Гайратович</h3>
              <p>Заместитель Директора по Проектам</p>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-6 about-me-person">
              <img src={team_1} alt="img" />
              <h3>Акабиров Абубакир Ибрагимович</h3>
              <p>Эксперт по Промышленному Безопасности </p>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3 col-md-3 col-sm-6 about-me-person">
              <img src={team_8} alt="img" />
              <h3>Курбанова Умида Абдуллаевна</h3>
              <p>Начальник Отдел Кадров</p>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-6 about-me-person">
              <img src={team_5} alt="img" />
              <h3>Хасидов Абдурахим</h3>
              <p> Главный Специалист по делам Строительства</p>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-6 about-me-person">
              <img src={team_6} alt="img" />
              <h3>Ганиев Анвар Ахатович</h3>
              <p>Главный Бухгалтер</p>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-6 about-me-person">
              <img src={team_7} alt="img" />
              <h3>Арифов Зинур</h3>
              <p>Инженер Строитель</p>
            </div>
          </div>
        </div>
        <Logo />
      </section>
    </>
  );
};

export default AboutMe;
