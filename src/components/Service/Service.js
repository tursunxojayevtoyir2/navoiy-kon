import React from "react";
import "./Service.scss";
import { Link } from "react-router-dom";

import image_1 from "../../image/mobelniy-konteyner.jpg";
import image_2 from "../../image/lesomantajnie.jpg";
import image_3 from "../../image/betonni-paboti.jpg";
import image_4 from "../../image/svarochnie.jpg";

const Service = () => {
  return (
    <section className="service-section">
      <div className="container">
        <h1 className="service-title"> Наши услуги</h1>
        <div className="row">
          <div className="col-6-lg col-md-6 col-sm-12">
            <div className="service-item">
              <img src={image_1} alt="img" />
              <h3>Строительные мобильные контейнеры</h3>
              <p>
                В большинстве случаев строительные площадки находятся далеко от
                городов и населенных пунктов, поэтому ООО «NAVOIY KONI SERVIS»
                имеет 20 мобильных контейнеров, в каждом из которых могут
                разместиться 16 человек.
              </p>
              <p className="read-more">
                <Link to="service">Подробнеe... </Link>{" "}
              </p>
            </div>
          </div>
          <div className="col-6-lg col-md-6 col-sm-12">
            <div className="service-item">
              <img src={image_2} alt="img" />
              <h3> Лесомонтажные pаботы</h3>
              <p>
                Мы имеем вместимость 20 километров (D40) труб, 5000 штук хомуты
                и 1000 платформы. И имеет команду из 50 квалифицированных
                рабочих, работающих на металлоконструкции.
              </p>
              <p className="read-more">
                <Link to="service">Подробнеe... </Link>{" "}
              </p>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-6-lg col-md-6 col-sm-12 ">
            <div className="service-item">
              <img src={image_3} alt="img" />
              <h3>Бетонные работы</h3>
              <p>
                Компания выполняет земляные работы, работы по строительству
                железобетонных и монолитных бетонов. Сейчас у нас сильная
                команда из 150 человек в этой сфере. Наша компания имеет
                мощность 2500 кубометров цементной смеси и оборудования.
              </p>
              <p className="read-more">
                <Link to="service">Подробнеe... </Link>{" "}
              </p>
            </div>
          </div>
          <div className="col-6-lg col-md-6 col-sm-12">
            <div className="service-item">
              <img src={image_4} alt="img" />
              <h3>Сварочные работы</h3>
              <p>
                ООО «НАВОИ КОНИ СЕРВИС» выполняет сварочные работы на объектах
                строительства повышенной опасности. Профессиональные сварщики
                имеют многолетний опыт и высокую квалификацию
              </p>
              <p className="read-more">
                <Link to="service">Подробнеe... </Link>{" "}
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Service;
