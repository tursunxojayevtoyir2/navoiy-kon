import React from 'react'
import './Logo.scss'
import logo_1 from '../../image/logo-1.jpg'
import logo_2 from '../../image/logo-2.jpg'

const Logo = () => {
  return (
    <>
    <section className="logo-section">
    <div className="container">
        <div className="row">
            <div className="col-lg-2 col-md-2 col-sm-2">
                <img src={logo_1} alt="" />
            </div>
            <div className="col-lg-2 col-md-2 col-sm-2">
                <img src={logo_2} alt="" />
            </div>
            <div className="col-lg-2 col-md-2 col-sm-4">
                <img src={logo_1} alt="" />
            </div>
            <div className="col-lg-2 col-md-2 col-sm-4">
                <img src={logo_2} alt="" />
            </div>
            <div className="col-lg-2 col-md-2 col-sm-4">
                <img src={logo_1} alt="" />
            </div>
            <div className="col-lg-2 col-md-2 col-sm-4">
                <img src={logo_2} alt="" />
            </div>
           
        </div>
    </div>
    </section>
    </>
  )
}

export default Logo