import React from "react";
import "./ServicesPage.scss";

import image_1 from "../../image/mobelniy-konteyner.jpg";
import image_2 from "../../image/lesomantajnie.jpg";
import image_3 from "../../image/betonni-paboti.jpg";
import image_4 from "../../image/svarochnie.jpg";
import image_5 from "../../image/svarshikov.jpg";
import main_2 from "../../image/Main-2.jpg"

const ServicesPage = () => {
  return (
    <>
      <section className="services-page-section">
        <div className="section-services-img">
          <img
            src={main_2}
            alt="img"
          />
        </div>
        <div className="container">
          <div className="desktop">
            <div className="row">
              <h1>Услуги</h1>
              <div className="col-7-lg col-md-7 col-sm-12 services-page-text">
                <h3>Строительные мобильные контейнеры</h3>
                <p>
                  В большинстве случаев строительные площадки находятся далеко
                  от городов и населенных пунктов, поэтому ООО «NAVOIY KONI
                  SERVIS» имеет 20 мобильных контейнеров, в каждом из которых
                  могут разместиться 16 человек. В целях повышения качества
                  строительства и постоянного обмена с объектом предусмотрены
                  передвижные офисы, передвижные кухни, передвижные санузлы.
                  Это, в свою очередь, облегчает жизнь и работу сотрудников в
                  труднодоступных районах
                </p>
              </div>
              <div className="col-5-lg col-md-5 col-sm-12 services-page-img">
                <img src={image_1} alt="img" />
              </div>
            </div>
            <div className="row">
              <div className="col-5-lg col-md-5 col-sm-12 services-page-img">
                <img src={image_2} alt="img" />
              </div>
              <div className="col-7-lg col-md-7 col-sm-12 services-page-text">
                <h3>Лесомонтажные работы</h3>
                <p>
                  Мы имеем вместимость 20 километров (D40) труб, 5000 штук
                  хомуты и 1000 платформы. И имеет команду из 50
                  квалифицированных рабочих, работающих на металлоконструкции.
                  Наша бригада по сборке металлических лесов также имеет опыт
                  работы на максимальной высоте 140 метров.
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-7-lg col-md-7 col-sm-12 services-page-text">
                <h3>Бетонные работы</h3>
                <p>
                  Компания выполняет земляные работы, работы по строительству
                  железобетонных и монолитных бетонов. Сейчас у нас сильная
                  команда из 150 человек в этой сфере. Наша компания имеет
                  мощность 2500 кубометров цементной смеси и оборудования.
                </p>
              </div>
              <div className="col-5-lg col-md-5 col-sm-12 services-page-img">
                <img src={image_3} alt="img" />
              </div>
            </div>
            <div className="row">
              <div className="col-5-lg col-md-5 col-sm-12 services-page-img">
                <img src={image_4} alt="img" />
              </div>
              <div className="col-7-lg col-md-7 col-sm-12 services-page-text">
                <h3>Сварочные работы</h3>
                <p>
                  ООО «НАВОИ КОНИ СЕРВИС» выполняет сварочные работы на объектах
                  строительства повышенной опасности. Профессиональные сварщики
                  имеют многолетний опыт и высокую квалификацию. Для аргоновой и
                  электрогазовой сварки у нас имеется 25 аргоносварочных
                  аппаратов, 15 электросварочных аппаратов, 3 аппарата
                  плазменной резки, 2 сварочных полуавтомата. А на объектах
                  повышенной опасности работает бригада из 50 сварщиков
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-7-lg col-md-7 col-sm-12 services-page-text">
                <h3>Подготовка сварщков</h3>
                <p>
                  ООО «НАВОИЙ КОНИ СЕРВИС» имеет специальный экзаменационный
                  корпус для практической проверки сварщиков. Основной задачей
                  этой системы является определение уровня
                  специалистов-сварщиков и проведение практических испытаний
                  новых сварщиков.
                </p>
              </div>
              <div className="col-5-lg col-md-5 col-sm-12 services-page-img">
                <img src={image_5} alt="img" />
              </div>
            </div>
          </div>
          <div className="mobile">
            <div className="row">
              <h1>Услуги</h1>
              <div className="col-7-lg col-md-7 col-sm-12 services-page-text">
                <h3>
                  <h3>Строительные мобильные контейнеры</h3>
                </h3>
                <p>
                  В большинстве случаев строительные площадки находятся далеко
                  от городов и населенных пунктов, поэтому ООО «NAVOIY KONI
                  SERVIS» имеет 20 мобильных контейнеров, в каждом из которых
                  могут разместиться 16 человек. В целях повышения качества
                  строительства и постоянного обмена с объектом предусмотрены
                  передвижные офисы, передвижные кухни, передвижные санузлы.
                  Это, в свою очередь, облегчает жизнь и работу сотрудников в
                  труднодоступных районах
                </p>
              </div>
              <div className="col-5-lg col-md-5 col-sm-12 services-page-img">
                <img src={image_1} alt="img" />
              </div>
            </div>
            <div className="row">
              <div className="col-7-lg col-md-7 col-sm-12 services-page-text">
                <h3>Лесомонтажные работы</h3>
                <p>
                  Мы имеем вместимость 20 километров (D40) труб, 5000 штук
                  хомуты и 1000 платформы. И имеет команду из 50
                  квалифицированных рабочих, работающих на металлоконструкции.
                  Наша бригада по сборке металлических лесов также имеет опыт
                  работы на максимальной высоте 140 метров.
                </p>
              </div>
              <div className="col-5-lg col-md-5 col-sm-12 services-page-img">
                <img src={image_2} alt="img" />
              </div>
            </div>
            <div className="row">
              <div className="col-7-lg col-md-7 col-sm-12 services-page-text">
                <h3>Бетонные работы</h3>
                <p>
                  Компания выполняет земляные работы, работы по строительству
                  железобетонных и монолитных бетонов. Сейчас у нас сильная
                  команда из 150 человек в этой сфере. Наша компания имеет
                  мощность 2500 кубометров цементной смеси и оборудования.
                </p>
              </div>
              <div className="col-5-lg col-md-5 col-sm-12 services-page-img">
                <img src={image_3} alt="img" />
              </div>
            </div>
            <div className="row">
              <div className="col-7-lg col-md-7 col-sm-12 services-page-text">
                <h3>Сварочные работы</h3>
                <p>
                  ООО «НАВОИ КОНИ СЕРВИС» выполняет сварочные работы на объектах
                  строительства повышенной опасности. Профессиональные сварщики
                  имеют многолетний опыт и высокую квалификацию. Для аргоновой и
                  электрогазовой сварки у нас имеется 25 аргоносварочных
                  аппаратов, 15 электросварочных аппаратов, 3 аппарата
                  плазменной резки, 2 сварочных полуавтомата. А на объектах
                  повышенной опасности работает бригада из 50 сварщиков
                </p>
              </div>
              <div className="col-5-lg col-md-5 col-sm-12 services-page-img">
                <img src={image_4} alt="img" />
              </div>
            </div>
            <div className="row">
              <div className="col-7-lg col-md-7 col-sm-12 services-page-text">
                <h3>Подготовка сварщков</h3>
                <p>
                  ООО «НАВОИЙ КОНИ СЕРВИС» имеет специальный экзаменационный
                  корпус для практической проверки сварщиков. Основной задачей
                  этой системы является определение уровня
                  специалистов-сварщиков и проведение практических испытаний
                  новых сварщиков.
                </p>
              </div>
              <div className="col-5-lg col-md-5 col-sm-12 services-page-img">
                <img src={image_5} alt="img" />
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default ServicesPage;
