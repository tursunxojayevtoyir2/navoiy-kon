import React from "react";
import "./Portfolio-three.scss";
import p_img_main_1 from "../../../image/portfoli-1-main.jpg";
import portfolio_3_main from "../../../image/portfolio-3-main.jpg";
import portfolio_3_1 from "../../../image/portfolio-3-1.jpg";
import portfolio_3_2 from "../../../image/portfolio-3-2.jpg";
import portfolio_3_3 from "../../../image/portfolio-3-3.jpg";
import portfolio_3_4 from "../../../image/portfolio-3-4.jpg";
import portfolio_3_5 from "../../../image/portfolio-3-5.jpg";
import portfolio_3_6 from "../../../image/portfolio-3-6.jpg";
import portfolio_3_7 from "../../../image/portfolio-3-7.jpg";
import portfolio_3_8 from "../../../image/portfolio-3-8.jpg";
import portfolio_3_9 from "../../../image/portfolio-3-9.jpg";
import portfolio_3_10 from "../../../image/portfolio-3-10.jpg";
import portfolio_3_11 from "../../../image/portfolio-3-11.jpg";
import portfolio_3_12 from "../../../image/portfolio-3-12.jpg";
import portfolio_3_13 from "../../../image/portfolio-3-13.jpg";
import portfolio_3_14 from "../../../image/portfolio-3-14.jpg";
import portfolio_3_15 from "../../../image/portfolio-3-15.jpg";
import portfolio_3_16 from "../../../image/portfolio-3-16.jpg";
import portfolio_3_17 from "../../../image/portfolio-3-17.jpg";
import portfolio_3_18 from "../../../image/portfolio-3-18.jpg";
import portfolio_3_19 from "../../../image/portfolio-3-19.jpg";
import portfolio_3_20 from "../../../image/portfolio-3-20.jpg";
import portfolio_3_21 from "../../../image/portfolio-3-21.jpg";
import portfolio_3_22 from "../../../image/portfolio-3-22.jpg";
import portfolio_3_23 from "../../../image/portfolio-3-23.jpg";
import portfolio_3_24 from "../../../image/portfolio-3-24.jpg";
import portfolio_3_25 from "../../../image/portfolio-3-25.jpg";
import portfolio_3_26 from "../../../image/portfolio-3-26.jpg";
import portfolio_3_27 from "../../../image/portfolio-3-27.jpg";
import portfolio_3_28 from "../../../image/portfolio-3-28.jpg";
import portfolio_3_29 from "../../../image/portfolio-3-29.jpg";
import portfolio_3_30 from "../../../image/portfolio-3-30.jpg";

const PortfolioThree = () => {
  return (
    <>
      <section className="portfolio-three-section">
        <div className="section-portfolio-page-img">
          <img src={p_img_main_1} alt="img" />
        </div>
        <div className="container">
          <div className="row">
            <h1>Наши проекты</h1>
            <div className="col-lg-8 col-md-8 col-sm-12">
              <div
                id="carouselExampleIndicators"
                className="carousel slide"
                data-bs-ride="carousel"
              >
                {/* <div className="carousel-indicators">
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="0"
                    aria-label="Slide 1"
                  ></button>
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="1"
                    aria-label="Slide 2"
                  ></button>
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="2"
                    aria-label="Slide 3"
                  ></button>
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="3"
                    className="active"
                    aria-current="true"
                    aria-label="Slide 4"
                  ></button>
                </div> */}
                <div className="carousel-inner">
                  <div className="carousel-item active">
                    <img src={portfolio_3_main} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_1} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_2} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_3} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_4} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_5} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_6} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_7} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_8} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_9} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_10} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_11} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_12} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_13} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_14} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_15} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_15} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_17} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_16} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_18} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_19} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_20} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_21} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_22} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_23} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_24} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_25} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_26} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_27} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_28} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_29} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_3_30} alt="img" />
                  </div>
                </div>
                <button
                  className="carousel-control-prev"
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide="prev"
                >
                  <span
                    className="carousel-control-prev-icon"
                    aria-hidden="true"
                  ></span>
                  <span className="visually-hidden">Previous</span>
                </button>
                <button
                  className="carousel-control-next"
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide="next"
                >
                  <span
                    className="carousel-control-next-icon"
                    aria-hidden="true"
                  ></span>
                  <span className="visually-hidden">Next</span>
                </button>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-12">
              <h2>Строительные работы в Химическом заводе</h2>
              <ul>
                <li>
                  <b>Год:</b> 2021 - 2022
                </li>
                <li>
                  <b>Место:</b> город Навои
                </li>
                <li>
                  <b>Заказчик:</b> JV Continaz
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default PortfolioThree;
