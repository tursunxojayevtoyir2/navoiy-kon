import React from "react";
import "./Portfolio-two.scss";
import p_img_main_1 from "../../../image/portfoli-1-main.jpg";
import portfolio_2_main from "../../../image/portfolio-2-main.jpg";
import portfolio_2_1 from "../../../image/portfolio-2-1.jpg";
import portfolio_2_2 from "../../../image/portfolio-2-2.jpg";
import portfolio_2_3 from "../../../image/portfolio-2-3.jpg";
import portfolio_2_4 from "../../../image/portfolio-2-4.jpg";
import portfolio_2_5 from "../../../image/portfolio-2-5.jpg";
import portfolio_2_6 from "../../../image/portfolio-2-6.jpg";
import portfolio_2_7 from "../../../image/portfolio-2-7.jpg";
import portfolio_2_8 from "../../../image/portfolio-2-8.jpg";
import portfolio_2_9 from "../../../image/portfolio-2-9.jpg";
import portfolio_2_10 from "../../../image/portfolio-2-10.jpg";
import portfolio_2_11 from "../../../image/portfolio-2-11.jpg";
import portfolio_2_12 from "../../../image/portfolio-2-12.jpg";
import portfolio_2_13 from "../../../image/portfolio-2-13.jpg";
import portfolio_2_14 from "../../../image/portfolio-2-14.jpg";
import portfolio_2_15 from "../../../image/portfolio-2-15.jpg";
import portfolio_2_16 from "../../../image/portfolio-2-16.jpg";
import portfolio_2_17 from "../../../image/portfolio-2-17.jpg";
import portfolio_2_18 from "../../../image/portfolio-2-18.jpg";
import portfolio_2_19 from "../../../image/portfolio-2-19.jpg";
import portfolio_2_20 from "../../../image/portfolio-2-20.jpg";
import portfolio_2_21 from "../../../image/portfolio-2-21.jpg";

const PortfolioTwo = () => {
  return (
    <>
      <section className="portfolio-two-section">
        <div className="section-portfolio-page-img">
          <img src={p_img_main_1} alt="img" />
        </div>
        <div className="container">
          <div className="row">
            <h1>Наши проекты</h1>
            <div className="col-lg-8 col-md-8 col-sm-12">
              <div
                id="carouselExampleIndicators"
                className="carousel slide"
                data-bs-ride="carousel"
              >
                {/* <div className="carousel-indicators">
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="0"
                    aria-label="Slide 1"
                  ></button>
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="1"
                    aria-label="Slide 2"
                  ></button>
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="2"
                    aria-label="Slide 3"
                  ></button>
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="3"
                    className="active"
                    aria-current="true"
                    aria-label="Slide 4"
                  ></button>
                </div> */}
                <div className="carousel-inner">
                  <div className="carousel-item active">
                    <img src={portfolio_2_main} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_1} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_2} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_3} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_4} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_5} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_6} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_7} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_8} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_9} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_10} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_11} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_12} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_13} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_14} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_15} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_15} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_17} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_16} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_18} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_19} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_20} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <img src={portfolio_2_21} alt="img" />
                  </div>
                </div>
                <button
                  className="carousel-control-prev"
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide="prev"
                >
                  <span
                    className="carousel-control-prev-icon"
                    aria-hidden="true"
                  ></span>
                  <span className="visually-hidden">Previous</span>
                </button>
                <button
                  className="carousel-control-next"
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide="next"
                >
                  <span
                    className="carousel-control-next-icon"
                    aria-hidden="true"
                  ></span>
                  <span className="visually-hidden">Next</span>
                </button>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-12">
              <h2>Строительные работы в Кизилкумцементе</h2>
              <ul>
                <li>
                  <b>Год:</b> 2022
                </li>
                <li>
                  <b>Место:</b> город Навои
                </li>
                <li>
                  <b>Заказчик:</b> Sinocemtech International Engineering Co.,LTD
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default PortfolioTwo;
