import React from "react";
import "./Portfolio.scss";
import { Link } from "react-router-dom";
import p_img_main_1 from "../../image/portfoli-1-main.jpg";
import p_img_main_2 from "../../image/portfolio-2-1.jpg";
import p_img_main_3 from "../../image/portfolio-3-main.jpg";

const Portfolio = () => {
  return (
    <>
      <section className="portfolio-section">
        <div className="container">
          <h1>Наши проекты</h1>
          <div className="row">
            <div className="col-lg-4 col-md-4 col-sm-12 col-div">
              <div>
                <img src={p_img_main_1} alt="images" />
                <h3>Строительные работы в проекте UNF </h3>
                <p className="read-more">
                  <Link to="portfolio">Подробнеe...</Link>
                </p>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-12 col-div">
              <div>
                <img src={p_img_main_2} alt="images" />
                <h3>Строительные работы в Кизилкумцементе </h3>
                <p className="read-more">
                  <Link to="portfolio">Подробнеe...</Link>
                </p>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-12 col-div">
              <div>
                <img
                  src={p_img_main_3}
                  alt="images"
                />
                <h3>Строительные работы в xимическом заводе</h3>
                <p className="read-more">
                  <Link to="portfolio">Подробнеe...</Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Portfolio;
