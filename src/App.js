import Navbar from "./components/Navbar/Navbar";
import Home from "./components/Home/Home";
import Footer from "./components/Footer/Footer";
import ServicesPage from "./components/ServicesPage/ServicesPage";
import AboutMe from "./components/AboutMe/AboutMe";
import PortfolioPage from "./components/PortfolioPage/PortfolioPage";
import PortfolioOne from "./components/AllPortfolio/Portfolio-one/Portfolio-one";
import Application from "./components/Application/Application";
import './App.css'
import { Routes, Route } from "react-router-dom";
import PortfolioTwo from "./components/AllPortfolio/Portfolio-two/Portfolio-two";
import PortfolioThree from "./components/AllPortfolio/Portfolio-three/Portfolio-three";

function App() {
  return (
    <>
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/service" element={<ServicesPage />} />
        <Route path="/about-me" element={<AboutMe />} />
        <Route path="/portfolio" element={<PortfolioPage/>} />
        <Route path="/portfolio-one" element={<PortfolioOne/>} />
        <Route path="/portfolio-two" element={<PortfolioTwo/>} />
        <Route path="/portfolio-three" element={<PortfolioThree/>} />
        <Route path="/application" element={<Application/>} />
      </Routes>
      <Footer />
    </>
  );
}

export default App;
